function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        $('.g-signin2').css("display", "block");
        $('.data').css("display", "none");
    });
    auth2.disconnect();
}

function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    csvFile = new Blob([csv], {type: "text/csv"});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("#table tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");

        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);

        csv.push(row.join(","));
    }

    download_csv(csv.join("\n"), filename);
}

document.querySelector("#submit").addEventListener("click", function () {
    var html = document.querySelector("#table").outerHTML;
    export_table_to_csv(html, "data.csv");
});

let simplepicker = new SimplePicker({
    zIndex: 10
});

simplepicker.open();

const $button = document.querySelector('button');
const $eventLog = document.querySelector('.event-log');
$button.addEventListener('click', (e) => {
    e.preventDefault();
    simplepicker.open();
});

simplepicker.on('submit', (date, readableDate) => {
    var str1 = readableDate;
    var res1 = str1.split(" ");
    var str2 = document.getElementById('test-time').value;
    var res2 = str2.split(" ");
    var eventLog = readableDate +' '+ res2 + '\n';
    var res3= eventLog.replace(/AM|PM/gi, "");

    $eventLog.innerHTML += res3;

    var str = res1 + res2 + '\n';

    var dt = readableDate + res2;

    var data =dt.replace(/AM|PM/gi, "").replace("January","1").replace("Febuary","2").replace("March","3").replace("April","4").replace("May","5").replace("June","6")
        .replace("July","7").replace("August","8").replace("September","9").replace("October","10").replace("November","11").replace("December","12");
    var split = data.split(" ");
    var ymd = split[2]+'-'+split[1]+'-'+split[0];
    var StartTime = split[3];
    var EndTime = split[4];
    var csv = ymd +' '+ StartTime +' '+ EndTime;

    var dataCSV = csv.split(" ");
    console.log(dataCSV);

    $("#table").append(
        "<tr>" +
        "<td>"+ymd+"</td>" +
        "<td>"+StartTime+"</td>" +
        "<td>"+EndTime+"</td>" +
        "</tr>"
    );

});