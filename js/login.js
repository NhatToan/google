function onSignIn(googleUser)
{
    var profile = googleUser.getBasicProfile();
    console.log(profile);
    $(".g-signin2").css("display", "none");
    $(".data").css("display", "block");
    $("#pic").attr('src', profile.getImageUrl());
    $("#email").text(profile.getEmail());
    $("#name").text(profile.getName());
}
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        $('.g-signin2').css("display", "block");
        $('.data').css("display", "none");
    });
    auth2.disconnect();
}